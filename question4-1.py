import numpy as np
import matplotlib.pyplot as plt


# 蒙特卡洛模拟函数
def monte_carlo_simulation(groups, P_dirty, M, acceptable_risk):
    np.random.seed(0)  # 设定随机种子
    team_flags_needed = np.ones(N_teams, dtype=int)  # 每支队伍至少需要一个旗帜
    org_flags_needed = 1  # 举办方至少需要一个旗帜
    sponsor_flags_needed = 3  # 赞助方至少需要三个旗帜

    # 计算每个小组的比赛数
    group_matches = [g * (g - 1) // 2 for g in groups]
    total_group_matches = sum(group_matches)

    # 计算淘汰赛的总比赛数
    knockout_matches = 16  # 1/8决赛(8) + 1/4决赛(4) + 半决赛(2) + 决赛(1)

    # 计算总比赛数
    total_matches = total_group_matches + knockout_matches

    # 模拟每支队伍的小组赛
    for _ in range(M):
        # 模拟每支队伍的旗帜使用情况
        for i in range(N_teams):
            flags = 1  # 每支队伍至少有一个旗帜
            dirty_flags = 0
            # 小组赛阶段
            group_index = next(index for index, size in enumerate(groups) if i < sum(groups[:index + 1]))
            matches = groups[group_index] - 1  # 每支队伍小组赛的比赛数
            dirty_flags += np.sum(np.random.rand(matches) < P_dirty)
            # 淘汰赛阶段（假设每队都有可能进入每个淘汰赛阶段）
            for stage in [8, 4, 2, 1]:  # 1/8决赛，1/4决赛，半决赛，决赛
                if np.random.rand() < 0.5 ** (np.log2(stage)):  # 随机决定是否晋级
                    if np.random.rand() < P_dirty:
                        dirty_flags += 1
            flags += dirty_flags
            team_flags_needed[i] = max(team_flags_needed[i], flags)

        # 模拟举办方和赞助方的旗帜使用情况
        no_org_dirty_flags = np.sum(np.random.rand(total_matches) >= P_dirty)
        no_sponsor_dirty_flags = np.sum(np.random.rand((total_matches) * 3) >= P_dirty)
        org_flags_needed = max(org_flags_needed, 1 + total_matches - no_org_dirty_flags)
        sponsor_flags_needed = max(sponsor_flags_needed, 3 + total_matches * 3 - no_sponsor_dirty_flags)

    # 为颁奖仪式增加旗帜
    # org_flags_needed += 1
    # sponsor_flags_needed += 3

    return team_flags_needed, org_flags_needed, sponsor_flags_needed


# 参数设定
N_teams = 21  # 队伍数
groups = [4, 4, 2, 3, 4, 4]  # 每个小组的队伍数
P_dirty = 0.1  # 假设旗帜污损概率为10%
M = 10000  # 蒙特卡洛模拟次数
acceptable_risk = 0.0001  # 可接受的风险水平

# 运行蒙特卡洛模拟
# team_flags, org_flags, sponsor_flags = monte_carlo_simulation(groups, P_dirty, M, acceptable_risk)
#
# # 输出结果
# print(f"每支队伍需要准备的旗帜数: {team_flags}")
# print(f"举办方需要准备的旗帜数: {org_flags}")
# print(f"赞助方需要准备的旗帜数: {sponsor_flags}")


# 参数设定
N_teams = 21  # 队伍数
groups = [4, 4, 2, 3, 4, 4]  # 每个小组的队伍数
M = 1000  # 蒙特卡洛模拟次数
plt.rcParams['font.sans-serif'] = ['SimHei']  # 用来正常显示中文标签


# 不同P_dirty下的旗帜需求（每场比赛P_dirty相同）
def simulate_flags():
    # P_dirty的范围，例如从0%到100%，步长为10%
    P_dirty_values = np.arange(0, 1.1, 0.1)
    total_team_flags_needed = []
    total_org_flags_needed = []
    total_sponsor_flags_needed = []

    # 对每个P_dirty值运行蒙特卡洛模拟
    for P_dirty in P_dirty_values:
        team_flags, org_flags, sponsor_flags = monte_carlo_simulation(groups, P_dirty, M, acceptable_risk)
        total_team_flags_needed.append(sum(team_flags))
        total_org_flags_needed.append(org_flags)
        total_sponsor_flags_needed.append(sponsor_flags)

    # 作图
    plt.plot(P_dirty_values, total_team_flags_needed, label='总队伍旗帜数')
    plt.plot(P_dirty_values, total_org_flags_needed, label='组织方旗帜数')
    plt.plot(P_dirty_values, total_sponsor_flags_needed, label='赞助方旗帜数')
    plt.plot(P_dirty_values, np.array(total_team_flags_needed) + total_org_flags_needed + total_sponsor_flags_needed,
             label='总旗帜数')

    plt.xlabel('旗帜污损率 (P_dirty)')
    plt.ylabel('旗帜需求数（个）')
    plt.title('不同旗帜污损率下的旗帜需求数')
    plt.legend()
    plt.grid(True)
    plt.show()


# 若P_dirty服从正态分布
# def simulate_flags_normal(N_teams, groups, P_dirty, M):
#     # P_dirty的范围，例如从0%到100%，步长为10%
#     P_dirty_values = np.arange(0, 1.1, 0.1)
#     total_team_flags_needed = []
#     total_org_flags_needed = []
#     total_sponsor_flags_needed = []
#
#     # 对每个P_dirty值运行蒙特卡洛模拟
#     for P_dirty in P_dirty_values:
#         team_flags, org_flags, sponsor_flags = monte_carlo_simulation(groups, P_dirty, M, acceptable_risk)
#         total_team_flags_needed.append(sum(team_flags))
#         total_org_flags_needed.append(org_flags)
#         total_sponsor_flags_needed.append(sponsor_flags)
#
#     # 作图
#     plt.plot(P_dirty_values, total_team_flags_needed, label='总队伍旗帜数')
#     plt.plot(P_dirty_values, total_org_flags_needed, label='组织方旗帜数')
#     plt.plot(P_dirty_values, total_sponsor_flags_needed, label='赞助方旗帜数')
#     plt.plot(P_dirty_values, np.array(total_team_flags_needed) + total_org_flags_needed + total_sponsor_flags_needed,
#              label='总旗帜数')
#
#     plt.xlabel('旗帜污损率 (P_dirty)')
#     plt.ylabel('旗帜需求数（个）')
#     plt.title('不同旗帜污损率下的旗帜需求数')
#     plt.legend()
#     plt.grid(True)
#     plt.show()


simulate_flags()
